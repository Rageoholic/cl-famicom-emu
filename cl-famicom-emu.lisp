;;;; cl-famicom-emu.lisp

(in-package #:cl-famicom-emu)

(deftype u8 () '(unsigned-byte 8))
(deftype u16 () '(unsigned-byte 16))

(defstruct mapper
  (mapper-number 0 :type (unsigned-byte 12))
  (submapper-number 0 :type (unsigned-byte 4))
  (prg-rom-size 16384 :type (integer 0))
  (chr-rom-size 8192 :type (integer 0))
  (prg-ram-size 0 :type (integer 0))
  (chr-ram-size 0 :type (integer 0))
  (prg-nvram-size 0 :type (integer 0))
  (chr-nvram-size 0 :type (integer 0))
  (mirroring-type :horizontal :type (keyword)))

(defparameter *ram* (make-array #x800 :element-type 'u8 :initial-element 0))

(defparameter prg-rom (make-array #x4000 :element-type 'u8 :initial-element 0 :adjustable t))

(defparameter chr-rom (make-array #x2000 :element-type 'u8 :initial-element 0 :adjustable t))

(defparameter mapper (make-mapper))

(defparameter cpu (make-cpu-6502 :decimal-mode-enabled nil))

(defmethod cl-6502-emu:6502-read-byte (cpu address)
  (cond ((< address #x2000) (aref *ram* (logand address #x7FF)))
        ((>= address #x8000) (aref prg-rom (logand address #x3FFF)))
        (t 0)))

(defmethod cl-6502-emu:6502-write-byte (cpu address data)
  (cond ((< address #x2000) (setf (aref *ram* (logand address #x7FF)) data))
        (t 0)))

(defmethod cl-6502-emu:6502-sync (cpu)
  (values))

(defun load-ines-file (filename)
  (with-open-file (stream filename :element-type 'u8)
    (file-position stream 4)
    (let* ((prg-rom-size-lsb (read-byte stream))
           (chr-rom-size-lsb (read-byte stream))
           (flags-6 (read-byte stream))
           (flags-7 (read-byte stream))
           (byte-8 (read-byte stream))
           (byte-9 (read-byte stream))
           (byte-10 (read-byte stream))
           (byte-11 (read-byte stream))
           (cpu-ppu-timing (read-byte stream))
           (hardware-type (read-byte stream)))
      (let* ((mapper (logior (ash (logand byte-8 #xf) 8) (logand flags-7 #xf0) (ash (logand flags-6 #xf0) -4)))
             (submapper (ash (logand byte-8 #xf0) -4))
             (prg-rom-size-msb (logand byte-9 #xf))
             (chr-rom-size-msb (ash (logand byte-9 #xf0) -4))
             (prg-rom-size 16384)
             (chr-rom-size 8192))
        (if (= prg-rom-size-msb #xf)
            (setf prg-rom-size (* (+ 1 (* 2 (logand prg-rom-size-lsb #x3))) (ash 1 (ash (logand prg-rom-size-lsb #xfc) -2))))
            (setf prg-rom-size (* 16384 (logior (ash prg-rom-size-msb 8) prg-rom-size-lsb))))
        (adjust-array prg-rom prg-rom-size :element-type 'u8 :initial-element 0)
        (file-position stream 16)
        (read-sequence prg-rom stream :start 0 :end prg-rom-size)
        (make-mapper :mapper-number mapper :submapper-number submapper :prg-rom-size prg-rom-size)))))
